__author__ = 'vsel'
def readInChunks(fileObj, chunkSize=2048):
    """
    Lazy function to read a file piece by piece.
    Default chunk size: 2kB.
    """
    while True:
        data = fileObj.read(chunkSize)
        if not data:
            break
        yield data


file = open('mail_google.py')
for chuck in readInChunks(file):
    print chuck
file.close()

for line in open('mail_google.py','r').readlines():
    print line
