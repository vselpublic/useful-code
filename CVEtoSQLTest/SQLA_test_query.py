# queries.py
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from SQLA_exp_db import Cve
 
engine = create_engine('sqlite:///cvebase.db', echo=True)
 
# create a Session
Session = sessionmaker(bind=engine)
session = Session()
 
# how to do a SELECT * (i.e. all)
res = session.query(Cve).all()
for cves in res:
    cves.showAll()
 