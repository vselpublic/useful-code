import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from SQLA_exp_db import Cve
 
#engine = create_engine('sqlite:///cvebase.db', echo=True)
engine = create_engine('sqlite:///cvebase.db')
  
# create a Session
Session = sessionmaker(bind=engine)
session = Session()

def insert_event(event):
    session.add(Cve(**event))
    session.commit()