import threading

def tr1(x, event_for_wait, event_for_set):
    event_for_wait.wait() # wait for event
    event_for_wait.clear()
    for i in xrange(10):
        print "start "+str(x)
        print x+i
        if (x+i+100)%3 == 0:
            event_for_set.set()
            event_for_wait.wait() # wait for event
            event_for_wait.clear() # clean event for future# set event for neighbor thread
            
def tr2(x, event_for_wait, event_for_set):
    event_for_wait.wait() # wait for event
    event_for_wait.clear() # clean event for future
    for i in xrange(100):
        print 100
        #print "start "+str(x)
        #print x+i
    event_for_set.set() # set event for neighbor thread

# init events
e1 = threading.Event()
e2 = threading.Event()


# init threads
t1 = threading.Thread(target=tr1, args=(0, e1, e2))
t2 = threading.Thread(target=tr2, args=(100, e2, e1))


# start threads
t1.start()
t2.start()

e1.set() # initiate the first event

# join threads to the main thread
t1.join()
t2.join()