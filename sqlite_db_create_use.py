__author__ = 'vsel'
import sqlite3
import sys

class CVEDB(object):
    """
    A simple class representing the cve vulnerability management system
    """
    def __init__(self):
        """
        Construct a new CVE VMS DB with defaults.
        """

        self.dbfile = "./cvedb"

        #NEED?
        #self.verbosity = 0

    def createDB(self):
        """
        Create a DB with the correct schema.
        """

        #NEED?
        #self.verbose("Creating new database as '%s'..." % self.dbfile)

        try:
            conn = sqlite3.connect(self.dbfile)
            c = conn.cursor()
            c.execute('''CREATE TABLE nvd (access_vector varchar,
                                            access_complexity varchar,
                                            authentication varchar,
                                            availability_impact varchar,
                                            confidentiality_impact varchar,
                                            cve_id text primary key,
                                            integrity_impact varchar,
                                            last_modified_datetime varchar,
                                            published_datetime varchar,
                                            score real,
                                            summary varchar,
                                            urls varchar,
                                            vulnerable_software_list)''')
            c.execute('''CREATE TABLE vulnerable_software (software_id varchar,
                                            part varchar,
                                            vendor varchar,
                                            product varchar,
                                            version varchar,
                                            revision varchar,
                                            edition varchar,
                                            language varchar,
                                            software_edition varchar,
                                            target_software varchar,
                                            target_hardware varchar,
                                            other  varchar)''')
            c.execute('''CREATE TABLE users (user_name varchar primary key,
                                            user_password_md5 varchar,
                                            user_role varchar)''')
            c.execute('''CREATE TABLE admins_vulnerability (cve_id text,
                                            status varchar,
                                            close_date date,
                                            description varchar,
                                            confirmed varchar,
                                            confirmed_date varchar)''')
            c.execute('''CREATE TABLE admins_vulnerable_software (software_id varchar,
                                            user_role varchar)''')
            conn.commit()
        except sqlite3.Error, e:
            #NEED?
            #sys.stderr.write("Unable to create DB file '%s'.\n" % self.dbfile)
            #sys.stderr.write("I'd love to tell you why, but the python sqlite3 module does not expose the error codes.\n")
            #sys.stderr.write("I'm guessing permissions problems.\n")
            sys.exit(1)

    def readTableByRow(self):
        """
        Read Table From SQLite DB by Row
        """
        conn = sqlite3.connect(self.dbfile)
        c = conn.cursor()
        for row in c.execute('SELECT * FROM nvd'):
           print row

#TESTS
#cvedb = CVEDB()
#cvedb.createDB()
#cvedb.readTableByRow()
