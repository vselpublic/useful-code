import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from SQLA_exp_db import Cve
 
engine = create_engine('sqlite:///cvebase.db', echo=True)
 
# create a Session
Session = sessionmaker(bind=engine)
session = Session()

#Create dict with CVE
elem = {'vendor': 'microsoft', 'severity': 'Low', 'app': 'windows_8', 'description': 'The Windows Error Reporting (WER) component in Microsoft Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows local users to bypass the Protected Process Light protection mechanism and read the contents of arbitrary process-memory locations by leveraging administrative privileges, aka "Windows Error Reporting Security Feature Bypass Vulnerability."', 'published': '2015-01-13', 'cvss': '1.9', 'name': 'CVE-2015-0001'}
# Add the record to the session object
session.add(Cve(**elem))
# commit the record the database
session.commit()