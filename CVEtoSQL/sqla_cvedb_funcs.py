import datetime
from SQLA_exp_db import Cve

def insert_event_to_sql(event,session):
    session.add(Cve(**event))
    session.commit()

def select_app(app,session):
    res = session.query(Cve).filter(Cve.app.like("%")).all()
    for cve in res:
        cve.showAll()