__author__ = 'vsel'
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
 
engine = create_engine('sqlite:///cvebase.db', echo=True)
Base = declarative_base()
 
########################################################################
class Cve(Base):
    """"""
    __tablename__ = "cvebase"
 
    id = Column(Integer, primary_key=True)
    name = Column(String)
    published = Column(Date)
    severity = Column(String)
    vendor = Column(String)
    app = Column(String)
    cvss = Column(String)
    description = Column(String)
 
    #----------------------------------------------------------------------
    def __init__(self, name, published, severity, vendor,app,cvss,description):
        """"""
        self.name = name
        self.published = published
        self.severity = severity
        self.vendor = vendor
        self.app = app
        self.cvss = cvss
        self.description = description
    
    def showAll(self):
        print self.name,self.published,self.severity,self.vendor,self.app,self.cvss,self.description
      
# create tables
Base.metadata.create_all(engine)