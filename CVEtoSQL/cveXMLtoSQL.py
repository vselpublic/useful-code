# coding: utf8
__author__ = 'vsel'
from lxml import etree
from datetime import datetime
from sqla_cvedb_funcs import insert_event_to_sql, select_app
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

#engine = create_engine('sqlite:///cvebase.db', echo=True)
engine = create_engine('sqlite:///cvebase.db')
  
# create a Session
Session = sessionmaker(bind=engine)
session = Session()

tree = etree.parse('nvdcve-2015.xml') # Парсинг файла
root = tree.getroot()



def insert_event(toor):
    a=[]
    for entry in toor.iter('{http://nvd.nist.gov/feeds/cve/1.2}entry'):
        #a.append(entry.attrib)
        for name in entry.iter('{http://nvd.nist.gov/feeds/cve/1.2}prod'):
            #print "ENTRY"
            #print entry.attrib.items()
            #print "NAME"
            #print name.attrib.get('name')
            for descript in  entry.iter('{http://nvd.nist.gov/feeds/cve/1.2}desc'):
                description = descript.find('{http://nvd.nist.gov/feeds/cve/1.2}descript').text
                event = {"name":entry.attrib.get('name'), "published":datetime.strptime(entry.attrib.get('published'),"%Y-%M-%d"),
                "severity":entry.attrib.get('severity'), "vendor":name.attrib.get('vendor'), "app":name.attrib.get('name'),
                "cvss":entry.attrib.get('CVSS_score'), "description":description}
                #print entry.attrib.get('name')
                insert_event_to_sql(event,session)
    #print len(a)
    #print a[0]
        

        

insert_event(root)
select_app("test",session)
