__author__ = 'vsel'
"""
CSV File Reader Writer
"""
import csv
import StringIO


class MyDialect(csv.Dialect):
    quotechar = '\x07'
    delimiter = ';'
    lineterminator = '\n'
    doublequote = False
    skipinitialspace = False
    quoting = csv.QUOTE_NONE
    escapechar = '\\'

def FromTxtToList(filename):
    reslist = [i.strip() for i in open(filename).readlines()]
    return reslist


in_file = open("F:/PyScripts/UPCGG1.csv", "rb")
reader = csv.reader(in_file, delimiter=';')
out_file = open("F:/PyScripts/UPCGG1_cleared.csv", "wb")
writer = csv.writer(out_file, dialect=MyDialect())
savedrow=["nope",0]
for row in reader:
    if savedrow[0][:7]!=row[0][:7]:
        writer.writerow(row)
        savedrow[0]=row[0]
in_file.close()
out_file.close()