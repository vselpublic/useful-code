__author__ = 'vsel'
import socket               # Import socket module

server = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 12345                # Reserve a port for your service.
server.bind((host, port))        # Bind to the port

server.listen(5)                 # Now wait for client connection.
while True:
   connection, addr = server.accept()     # Establish connection with client.
   print 'Got connection from', addr
   connection.send('Thank you for connecting')
   connection.close()                # Close the connection