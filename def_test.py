__author__ = 'vsel'
import little_crypter
import unittest
from Crypto.Cipher import AES
from Crypto.Hash import MD5

class TestCryptFunctions(unittest.TestCase):
    def setUp(self):
        m = MD5.new()
        m.update('abc')
        key=m.hexdigest()
        iv = "1234567890ABCDEF"
        self.crypted = little_crypter.crypt("The answer is no",key,iv)
        self.crypted2 = little_crypter.crypt("The answer is n1",key,iv)

    def test_crypted(self):
        self.assertTrue (self.crypted == self.crypted2, "Fee function returns incorrent value.")

if __name__ == '__main__':
    unittest.main()