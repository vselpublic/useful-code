import gmpy2
from gmpy2 import xmpz, log2, t_mod, mpz, bit_length

#according to https://primes.utm.edu/lists/2small/0bit.html
#64bit
#xmpz(2**65-49),xmpz(2**65-79)
#128bit
#xmpz(2**128-159)

import random

def decompose(n):
   exponentOfTwo = 0

   while n % 2 == 0:
      n = n/2
      exponentOfTwo += 1

   return exponentOfTwo, n

def isWitness(possibleWitness, p, exponent, remainder):
   possibleWitness = pow(possibleWitness, remainder, p)

   if possibleWitness == 1 or possibleWitness == p - 1:
      return False

   for _ in range(exponent):
      possibleWitness = pow(possibleWitness, 2, p)

      if possibleWitness == p - 1:
         return False

   return True

def probablyPrime(p, accuracy=1000):
   if p == 2 or p == 3: return True
   if p < 2: return False

   numTries = 0
   exponent, remainder = decompose(p - 1)

   for _ in range(accuracy):
      possibleWitness = random.randint(2, p - 2)
      if isWitness(possibleWitness, p, exponent, remainder):
         return False

   return True

def pryme_range(start, end, step):
    while start <= end:
        yield start
        start += step

def findPrimeFromPrimeOLD(a):
    minimalNumber=xmpz(2**a)
    maximumNumber=xmpz(2**(a+1))
    #print minimalNumber
    #print maximumNumber
    primeList = []
    primeListX2 = []
    for isPrime in pryme_range(minimalNumber,maximumNumber,1):
        if probablyPrime(isPrime):
            primeList.append(isPrime)
            #print primeList
            if len(primeList)>1:
                for prime in primeList:
                    #print prime
                    if len(primeList)>=primeList.index(prime):
                        n=prime*primeList[primeList.index(prime)+1]
                        if n not in primeListX2:
                            primeListX2.append(n)
                            if len(primeListX2)>20 :return primeListX2
def gordonPart(s,t):
    for l in range(1, log2(t)):
        r=2*l*t+1
        if probablyPrime(r):
            break
    #print map(type,(s,r,t))
    #u = t_mod(s**(r-1)-r**(s-1),r*s)
    u = t_mod(pow(s,r-1,r*s)-pow(r,s-1,r*s),r*s)
    if u%2 == 0:
        p0 = u + r*s
    else: p0 = u
    for k in range(1, log2(t)):
        p=p0+2*k*r*s
        if probablyPrime(p):
            return p
    return False
    
def findStongPrimeFromPrime(a):
    minimalNumber=xmpz(2**(a/2-a/16))
    maximumNumber=xmpz(2**((a/2-a/16)+1))
    #print minimalNumber
    #print maximumNumber
    primeList = []
    strongPrimeList = []
    for isPrime in pryme_range(minimalNumber,maximumNumber,1):
        if probablyPrime(isPrime):
            primeList.append(isPrime)
            #print primeList
            if len(primeList)>1:
                for prime in primeList:
                    #print prime
                    if len(primeList)>=primeList.index(prime):
                        #print primeList
                        p = gordonPart(s=prime,t=primeList[primeList.index(prime)+1])
                        if p != False:
                            #print p
                            #print(bit_length(p))
                            if p not in strongPrimeList and bit_length(p)>=(a-2) \
                            and bit_length(p)<=(a+4):
                                strongPrimeList.append(p)
                                if len(strongPrimeList) > 14:
                                    #print len(strongPrimeList)
                                    return strongPrimeList
a = findStongPrimeFromPrime(43)
print map(bit_length,a)
print a
#print bin(xmpz(340282366920938466451747147372715579809))
#print a[0]*a[1]
for p in a:
    for p2 in a:
        print map(bit_length,(p,p2))
        print p*p2
    