__author__ = 'vsel'
from Crypto.Cipher import AES
from Crypto.Hash import MD5
m = MD5.new()
m.update('abc')
key=m.hexdigest()
iv = "1234567890ABCDEF"
def crypt(crypt_block, key, iv):
    cipher_encrypt = AES.new(key, AES.MODE_CBC, iv)
    cipher_block = cipher_encrypt.encrypt(crypt_block)
    return cipher_block
    
def decrypt(uncrypt_block, key, iv):
    cipher_decrypt = AES.new(key, AES.MODE_CBC, iv)
    decrypt_block = cipher_decrypt.decrypt(uncrypt_block)
    return decrypt_block

def crypt_file(filepath,key,iv):
    file_to_crypt = open(filepath,"r")
    full_file = file_to_crypt.read()
    while len(full_file)%16!=0:
        full_file = full_file+b'0'
    file_crypted = open(filepath+".cr", "a")
    file_crypted.write(crypt(full_file,key,iv))
    file_crypted.close()
    file_to_crypt.close()
    
def decrypt_file(filepath,key,iv):
    file_crypted = open(filepath, "r")
    file_unencrypted = open(filepath[:7]+"un.jpg", "a")
    full_file = file_crypted.read()
    file_unencrypted.write(decrypt(full_file,key,iv))
    file_crypted.close()
    file_unencrypted.close()
    
    
def readInChunks(fileObj, chunkSize=2048):
    """
    Lazy function to read a file piece by piece.
    Default chunk size: 2kB.
    """
    while True:
        data = fileObj.read(chunkSize)
        if len(data)<16 and len(data)>0:
            data = data+b'0'
            print len(data)
        if not data:
            break
        yield data

crypt_file('Patern_test.jpg',key,iv)
decrypt_file('Patern_test.jpg.cr',key,iv)